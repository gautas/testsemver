# testsemver

Testing [semver-release](https://github.com/semantic-release/semantic-release)

Step 1: add the ci config
From https://github.com/semantic-release/semantic-release/blob/master/docs/recipes/ci-configurations/gitlab-ci.md

- need to add main as a release branch in .releaserc
- need to add a gitlab pat as ci variable, pass to the stage as GITLAB_TOKEN